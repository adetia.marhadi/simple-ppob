package simpleppob.dao;

import java.math.BigDecimal;
import org.hsqldb.util.DatabaseManagerSwing;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import com.fasterxml.jackson.core.JsonProcessingException;
import simpleppob.dao.impl.TransactionDaoImpl;
import simpleppob.dao.impl.TransactionDetailDaoImpl;
import simpleppob.dao.impl.UserDaoImpl;
import simpleppob.dto.InquiryDto;
import simpleppob.dto.PaymentDto;
import simpleppob.dto.ProductDto;
import simpleppob.json.InquiryDataRequest;
import simpleppob.json.SaveUserRequest;

public class TransactionTest {

  private EmbeddedDatabase db;

  @Before
  public void setup() {
    db = new EmbeddedDatabaseBuilder().setType(EmbeddedDatabaseType.HSQL)
        .addScript("db/sql/create.sql").addScript("db/sql/insert.sql").build();
  }

  @Test
  public void saveTransactionTest() {
    NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(db);

    TransactionDaoImpl transactionDaoImpl = new TransactionDaoImpl();
    transactionDaoImpl.setNamedParameterJdbcTemplate(jdbcTemplate);

    InquiryDataRequest inquiryData = new InquiryDataRequest();
    inquiryData.setProductCode("E001");
    inquiryData.setEmail("adetia.marhadi@mail.com");
    inquiryData.setPassword("adetest");
    inquiryData.setNominal(BigDecimal.valueOf(10000));
    inquiryData.setTransactionCode("TR001");

    ProductDto productData = new ProductDto();
    productData.setPrice(BigDecimal.valueOf(1750));
    productData.setSupplierCode("S001");
    productData.setToken("163yghjknqhwve61723");

    int save = transactionDaoImpl.save(inquiryData, productData);

    Assert.assertTrue(save > 0);
  }

  @Test
  public void saveTransactionDetailTest() throws JsonProcessingException {
    saveTransactionTest();

    NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(db);

    TransactionDetailDaoImpl transactionDetailDaoImpl = new TransactionDetailDaoImpl();
    transactionDetailDaoImpl.setNamedParameterJdbcTemplate(jdbcTemplate);

    InquiryDataRequest inquiryData = new InquiryDataRequest();
    inquiryData.setProductCode("E001");
    inquiryData.setEmail("adetia.marhadi@mail.com");
    inquiryData.setPassword("adetest");
    inquiryData.setNominal(BigDecimal.valueOf(10000));
    inquiryData.setTransactionCode("TR001");

    int save = transactionDetailDaoImpl.save(inquiryData, "INQUIRY");

    Assert.assertTrue(save > 0);
  }

  @Test
  public void findInquiryByTransactionCodeTest() {
    saveTransactionTest();

    NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(db);

    TransactionDaoImpl transactionDaoImpl = new TransactionDaoImpl();
    transactionDaoImpl.setNamedParameterJdbcTemplate(jdbcTemplate);

    InquiryDto inquiryDto = transactionDaoImpl.findInquiry("TR001");

    Assert.assertNotNull(inquiryDto);
    Assert.assertEquals("TR001", inquiryDto.getTransactionCode());
    Assert.assertEquals("S001", inquiryDto.getSupplierCode());
    Assert.assertEquals("E001", inquiryDto.getProductCode());
    Assert.assertEquals("163yghjknqhwve61723", inquiryDto.getToken());
  }

  @Test(expected = EmptyResultDataAccessException.class)
  public void findInquiryByTransactionCodeNotFoundTest() {
    NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(db);

    TransactionDaoImpl transactionDaoImpl = new TransactionDaoImpl();
    transactionDaoImpl.setNamedParameterJdbcTemplate(jdbcTemplate);

    transactionDaoImpl.findInquiry("TR002");
  }

  @Test
  public void updateStateTest() {
    saveTransactionTest();

    NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(db);

    TransactionDaoImpl transactionDaoImpl = new TransactionDaoImpl();
    transactionDaoImpl.setNamedParameterJdbcTemplate(jdbcTemplate);

    int update = transactionDaoImpl.updateState("TR001", "PAYMENT");

    Assert.assertTrue(update > 0);
  }

  @Test
  public void findPaymentTest() {
    NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(db);

    UserDaoImpl userDao = new UserDaoImpl();
    userDao.setNamedParameterJdbcTemplate(jdbcTemplate);

    SaveUserRequest user = new SaveUserRequest();
    user.setEmail("adetia.marhadi@mail.com");
    user.setPassword("adetest");

    userDao.save(user);

    updateStateTest();

    TransactionDaoImpl transactionDaoImpl = new TransactionDaoImpl();
    transactionDaoImpl.setNamedParameterJdbcTemplate(jdbcTemplate);

    PaymentDto paymentDto = transactionDaoImpl.findPayment("TR001");

    Assert.assertNotNull(paymentDto);
  }

  @After
  public void done() {
    // DatabaseManagerSwing.main(new String[] { "--url", "jdbc:hsqldb:mem:testdb", "--user", "sa",
    // "--password", "" });

    db.shutdown();
  }

}
