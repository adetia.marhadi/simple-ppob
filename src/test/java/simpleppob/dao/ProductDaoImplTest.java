package simpleppob.dao;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import simpleppob.dao.impl.ProductDaoImpl;
import simpleppob.dto.ProductDto;

public class ProductDaoImplTest {

  private EmbeddedDatabase db;

  @Before
  public void setup() {
    db = new EmbeddedDatabaseBuilder().setType(EmbeddedDatabaseType.HSQL)
        .addScript("db/sql/create.sql").addScript("db/sql/insert.sql").build();
  }

  @Test
  public void findByProductCodeTest() {
    NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(db);
    ProductDaoImpl productDao = new ProductDaoImpl();
    productDao.setNamedParameterJdbcTemplate(jdbcTemplate);

    ProductDto product = productDao.findByProductCode("E001");

    Assert.assertNotNull(product);
    Assert.assertEquals("E001", product.getProductCode());
    Assert.assertEquals("S001", product.getSupplierCode());
    Assert.assertEquals(1750d, product.getPrice().doubleValue(), 0d);
  }

  @Test(expected = EmptyResultDataAccessException.class)
  public void findByProductCodeNotFoundTest() {
    NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(db);
    ProductDaoImpl productDao = new ProductDaoImpl();
    productDao.setNamedParameterJdbcTemplate(jdbcTemplate);

    productDao.findByProductCode("E111");
  }

  @After
  public void done() {
    db.shutdown();
  }

}
