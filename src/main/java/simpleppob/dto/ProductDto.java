package simpleppob.dto;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class ProductDto {

  private String productCode;
  private String supplierCode;
  private BigDecimal price;
  private String token;

}
