package simpleppob.dto;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class UserDto {

  private int id;
  private String email;
  private String password;
  private BigDecimal balance;
  private String timestamp;

}
