package simpleppob.json;

import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class PayDataRequest {

  @JsonProperty("product")
  private String productCode;
  @JsonProperty("supplier")
  private String supplierCode;
  private String token;
  private String email;
  private String password;
  private BigDecimal nominal;
  @JsonProperty("transactionid")
  private String transactionCode;

}
