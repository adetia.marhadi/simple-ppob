package simpleppob.json;

import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class InquiryDataRequest {

  @JsonProperty("product")
  private String productCode;
  private String email;
  @JsonIgnore
  private String password;
  private BigDecimal nominal;
  @JsonProperty("transactionid")
  private String transactionCode;

}
