package simpleppob.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import simpleppob.service.CommitService;

@RestController
public class CommitController {

  @Autowired
  CommitService commitService;

  @PostMapping(path = "/commit/{code}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> commit(@PathVariable("code") String transactionCode) {
    return commitService.commit(transactionCode);
  }

}
