package simpleppob.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import simpleppob.json.SaveUserRequest;
import simpleppob.service.UserService;

@RestController
public class UserController {

  @Autowired
  UserService userService;

  @PostMapping(path = "/user", consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> saveUser(@RequestBody SaveUserRequest userRequest) {
    return userService.save(userRequest);
  }

}
