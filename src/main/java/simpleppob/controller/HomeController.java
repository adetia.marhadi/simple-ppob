package simpleppob.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import simpleppob.helper.CustomResponseHelper;

@RestController
public class HomeController {

  @GetMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> index() {
    return CustomResponseHelper.create(HttpStatus.OK);
  }

}
