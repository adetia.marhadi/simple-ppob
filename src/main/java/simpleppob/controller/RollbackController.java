package simpleppob.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import simpleppob.service.RollbackService;

@RestController
public class RollbackController {

  @Autowired
  RollbackService rollbackService;

  @PostMapping(path = "/rollback/{code}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> rollback(@PathVariable("code") String transactionCode) {
    return rollbackService.rollback(transactionCode);
  }

}
