package simpleppob.service;

import org.springframework.http.ResponseEntity;
import simpleppob.json.InquiryDataRequest;

public interface InquiryService {

  ResponseEntity<Object> inquiry(InquiryDataRequest inquiryData);

}
