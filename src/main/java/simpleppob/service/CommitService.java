package simpleppob.service;

import org.springframework.http.ResponseEntity;

public interface CommitService {

  ResponseEntity<Object> commit(String transactionCode);

}
