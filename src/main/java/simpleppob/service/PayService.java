package simpleppob.service;

import org.springframework.http.ResponseEntity;
import simpleppob.json.PayDataRequest;

public interface PayService {

  ResponseEntity<Object> pay(final PayDataRequest payData);

}
