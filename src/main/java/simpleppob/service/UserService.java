package simpleppob.service;

import org.springframework.http.ResponseEntity;
import simpleppob.json.SaveUserRequest;

public interface UserService {

  ResponseEntity<Object> save(final SaveUserRequest userRequest);

  ResponseEntity<Object> find(final String email);

}
