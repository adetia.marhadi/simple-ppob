package simpleppob.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import simpleppob.dao.ProductDao;
import simpleppob.dao.UserDao;
import simpleppob.dto.InquiryDto;
import simpleppob.dto.ProductDto;
import simpleppob.helper.CustomResponseHelper;
import simpleppob.json.InquiryDataRequest;
import simpleppob.service.InquiryService;
import simpleppob.service.JsonValidate;
import simpleppob.service.TransactionDetailService;
import simpleppob.service.TransactionService;
import simpleppob.type.TransactionState;

@Service
public class InquiryServiceImpl implements InquiryService, JsonValidate {

  private static final Logger LOGGER = LoggerFactory.getLogger(InquiryServiceImpl.class);

  private static final String EMAIL_NOT_FOUND = "Email not found.";

  @Autowired
  UserDao userDao;

  @Autowired
  ProductDao productDao;

  @Autowired
  TransactionService transactionService;

  @Autowired
  TransactionDetailService transactionDetailService;

  private PlatformTransactionManager transactionManager;

  @Autowired
  public void setTransactionManager(PlatformTransactionManager transactionManager) {
    this.transactionManager = transactionManager;
  }

  @Override
  public ResponseEntity<Object> inquiry(InquiryDataRequest inquiryData){
    TransactionDefinition def = new DefaultTransactionDefinition();
    TransactionStatus status = transactionManager.getTransaction(def);
    try {
      validate(inquiryData, "/inquiry.json");

      // check user
      findUser(inquiryData);

      // find product
      ProductDto product = findProductCode(inquiryData);

      // save transaction
      save(inquiryData, product);

      // find transaction
      InquiryDto inquiry = find(inquiryData);

      // save detail
      transactionDetailService.save(inquiryData, TransactionState.INQUIRY.toString());

      transactionManager.commit(status);

      return new ResponseEntity<>(inquiry, HttpStatus.OK);
    } catch (EmptyResultDataAccessException em) {
      LOGGER.error(em.getMessage(), em);
      return CustomResponseHelper.create(HttpStatus.NOT_FOUND, em.getMessage());
    } catch (IllegalArgumentException i) {
      LOGGER.error(i.getMessage(), i);
      return CustomResponseHelper.create(HttpStatus.BAD_REQUEST, i.getMessage());
    } catch (Exception e) {
      transactionManager.rollback(status);
      LOGGER.error(e.getMessage(), e);
      return CustomResponseHelper.create(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  private ProductDto findProductCode(InquiryDataRequest inquiryData) {
    ProductDto product;
    try {
      product = productDao.findByProductCode(inquiryData.getProductCode());
    } catch (EmptyResultDataAccessException e) {
      throw new EmptyResultDataAccessException("product not found.", 1, e);
    }
    return product;
  }

  private InquiryDto find(InquiryDataRequest inquiryData) {
    InquiryDto inquiry;
    try {
      inquiry = transactionService.findInquiry(inquiryData.getTransactionCode());
    } catch (EmptyResultDataAccessException e) {
      throw new EmptyResultDataAccessException("inquiry data not found.", 1, e);
    }
    return inquiry;
  }

  private void findUser(InquiryDataRequest inquiryData) {
    try {
      userDao.find(inquiryData.getEmail());
    } catch (EmptyResultDataAccessException e) {
      throw new EmptyResultDataAccessException(EMAIL_NOT_FOUND, 1, e);
    }
  }

  private void save(InquiryDataRequest inquiryData, ProductDto product) throws Exception {
    try {
      transactionService.save(inquiryData, product);
    } catch (DuplicateKeyException d) {
      throw new Exception("double inquiry.", d);
    }
  }

}
