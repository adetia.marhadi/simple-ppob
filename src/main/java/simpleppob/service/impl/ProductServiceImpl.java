package simpleppob.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import simpleppob.dao.ProductDao;
import simpleppob.dto.ProductDto;
import simpleppob.helper.CustomResponseHelper;
import simpleppob.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {

  private static final Logger LOGGER = LoggerFactory.getLogger(ProductServiceImpl.class);

  @Autowired
  ProductDao productDao;

  @Override
  public ResponseEntity<Object> find(String productCode) {
    try {
      if (null == productCode) {
        return CustomResponseHelper.create(HttpStatus.BAD_REQUEST);
      }

      ProductDto productDto = productDao.findByProductCode(productCode);

      return new ResponseEntity<>(productDto, HttpStatus.OK);
    } catch (EmptyResultDataAccessException e) {
      LOGGER.error(e.getMessage(), e);
      return CustomResponseHelper.create(HttpStatus.NOT_FOUND);
    } catch (Exception e) {
      LOGGER.error(e.getMessage(), e);
      return CustomResponseHelper.create(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

}
