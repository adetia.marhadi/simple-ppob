package simpleppob.service.impl;

import java.math.BigDecimal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import simpleppob.dao.TransactionDao;
import simpleppob.dao.TransactionDetailDao;
import simpleppob.dao.UserDao;
import simpleppob.dto.InquiryDto;
import simpleppob.dto.PaymentDto;
import simpleppob.helper.CustomResponseHelper;
import simpleppob.json.InquiryDataRequest;
import simpleppob.json.PayDataRequest;
import simpleppob.service.JsonValidate;
import simpleppob.service.PayService;
import simpleppob.type.TransactionState;

@Service
public class PayServiceImpl implements PayService, JsonValidate {

  private static final Logger LOGGER = LoggerFactory.getLogger(PayServiceImpl.class);

  @Autowired
  UserDao userDao;

  @Autowired
  TransactionDao transactionDao;

  @Autowired
  TransactionDetailDao transactionDetailDao;

  private PlatformTransactionManager transactionManager;

  @Autowired
  public void setTransactionManager(PlatformTransactionManager transactionManager) {
    this.transactionManager = transactionManager;
  }

  @Override
  public ResponseEntity<Object> pay(PayDataRequest payData) {
    TransactionDefinition def = new DefaultTransactionDefinition();
    TransactionStatus status = transactionManager.getTransaction(def);
    try {
      validate(payData, "/pay.json");

      // validate password
      validatePassword(payData);

      // validate pay
      InquiryDto inquiry = findInquiry(payData);
      if (payData.getProductCode().equals(inquiry.getProductCode())
          && payData.getSupplierCode().equals(inquiry.getSupplierCode())
          && payData.getToken().equals(inquiry.getToken())
          && payData.getEmail().equals(inquiry.getEmail())) {

        // store detail
        InquiryDataRequest inquiryData = new InquiryDataRequest();
        inquiryData.setEmail(inquiry.getEmail());

        BigDecimal nominal = inquiry.getTotal().subtract(inquiry.getFee());
        inquiryData.setNominal(nominal);
        inquiryData.setProductCode(inquiry.getProductCode());
        inquiryData.setTransactionCode(inquiry.getTransactionCode());

        transactionDetailDao.save(inquiryData, TransactionState.PAYMENT.toString());

        // deduct balance
        userDao.updateBalance(inquiry.getEmail(),
            inquiry.getTotal().multiply(BigDecimal.valueOf(-1)));

        // check last balance
        BigDecimal lastBalance = userDao.findBalance(inquiry.getEmail());
        if (lastBalance.compareTo(BigDecimal.ZERO) < 0) {
          this.transactionManager.rollback(status);
          return CustomResponseHelper.create(HttpStatus.BAD_REQUEST, "your balance is not enough.");
        }

        // update state to payment
        transactionDao.updateState(inquiry.getTransactionCode(),
            TransactionState.PAYMENT.toString());

        this.transactionManager.commit(status);
        
        PaymentDto payment = find(payData);

        return new ResponseEntity<>(payment, HttpStatus.OK);
      } else {
        return CustomResponseHelper.create(HttpStatus.NOT_FOUND);
      }
    } catch (EmptyResultDataAccessException e) {
      LOGGER.error(e.getMessage(), e);
      return CustomResponseHelper.create(HttpStatus.BAD_REQUEST,
          e.getMessage());
    } catch (IllegalArgumentException i) {
      LOGGER.error(i.getMessage(), i);
      return CustomResponseHelper.create(HttpStatus.BAD_REQUEST, i.getMessage());
    } catch (Exception e) {
      this.transactionManager.rollback(status);
      LOGGER.error(e.getMessage(), e);
      return CustomResponseHelper.create(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  private PaymentDto find(PayDataRequest payData) {
    PaymentDto payment;
    try {
      payment = transactionDao.findPayment(payData.getTransactionCode());
    } catch (EmptyResultDataAccessException e) {
      throw new EmptyResultDataAccessException("payment data not found.", 1, e);
    }
    return payment;
  }

  private InquiryDto findInquiry(PayDataRequest payData) {
    InquiryDto inquiry;
    try {
      inquiry = transactionDao.findInquiry(payData.getTransactionCode());
    } catch (EmptyResultDataAccessException e) {
      throw new EmptyResultDataAccessException("inquiry data not found.", 1, e);
    }
    return inquiry;
  }

  private void validatePassword(PayDataRequest payData) {
    try {
      userDao.validatePassword(payData.getEmail(), payData.getPassword());
    } catch (EmptyResultDataAccessException e) {
      throw new EmptyResultDataAccessException("username or password is incorrect.", 1, e);
    }
  }

}
