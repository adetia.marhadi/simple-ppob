package simpleppob.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import simpleppob.dao.TransactionDao;
import simpleppob.dto.InquiryDto;
import simpleppob.dto.ProductDto;
import simpleppob.json.InquiryDataRequest;
import simpleppob.service.TransactionService;

@Service
public class TransactionServiceImpl implements TransactionService {

  @Autowired
  TransactionDao transactionDao;

  @Override
  public int save(InquiryDataRequest inquiryData, ProductDto product) {
    return transactionDao.save(inquiryData, product);
  }

  @Override
  public InquiryDto findInquiry(String transactionCode) {
    return transactionDao.findInquiry(transactionCode);
  }

}
