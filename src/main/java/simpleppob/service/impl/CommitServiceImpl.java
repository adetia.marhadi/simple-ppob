package simpleppob.service.impl;

import java.math.BigDecimal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import simpleppob.dao.TransactionDao;
import simpleppob.dao.TransactionDetailDao;
import simpleppob.dao.UserDao;
import simpleppob.dto.PaymentDto;
import simpleppob.dto.TransactionStateDto;
import simpleppob.helper.CustomResponseHelper;
import simpleppob.json.InquiryDataRequest;
import simpleppob.service.CommitService;
import simpleppob.type.TransactionState;

@Service
public class CommitServiceImpl implements CommitService {

  private static final Logger LOGGER = LoggerFactory.getLogger(CommitServiceImpl.class);

  @Autowired
  TransactionDao transactionDao;

  @Autowired
  TransactionDetailDao transactionDetailDao;

  @Autowired
  UserDao userDao;

  private PlatformTransactionManager transactionManager;

  @Autowired
  public void setTransactionManager(PlatformTransactionManager transactionManager) {
    this.transactionManager = transactionManager;
  }

  @Override
  public ResponseEntity<Object> commit(String transactionCode) {
    if (null == transactionCode) {
      return CustomResponseHelper.create(HttpStatus.BAD_REQUEST);
    }

    TransactionDefinition def = new DefaultTransactionDefinition();
    TransactionStatus status = transactionManager.getTransaction(def);
    try {
      // find transaction with payment state
      PaymentDto payment = transactionDao.find(transactionCode);

      // if state is not payment state, do nothing
      TransactionState state = TransactionState.valueOf(payment.getState());
      if (TransactionState.COMMIT == state) {
        return CustomResponseHelper.create(HttpStatus.BAD_REQUEST,
            "the transaction has already been committed.");
      } else if (TransactionState.ROLLBACK == state) {
        return CustomResponseHelper.create(HttpStatus.BAD_REQUEST,
            "can not commit a rollback transaction.");
      } else if (TransactionState.INQUIRY == state) {
        return CustomResponseHelper.create(HttpStatus.PAYMENT_REQUIRED, "make payment first.");
      }

      // change to commit state
      transactionDao.updateState(transactionCode, TransactionState.COMMIT.toString());

      // save to detail transaction
      InquiryDataRequest transactionDetailData = new InquiryDataRequest();
      transactionDetailData.setEmail(payment.getEmail());

      BigDecimal nominal = payment.getTotal().subtract(payment.getFee());
      transactionDetailData.setNominal(nominal);
      transactionDetailData.setProductCode(payment.getProductCode());
      transactionDetailData.setTransactionCode(transactionCode);

      transactionDetailDao.save(transactionDetailData, TransactionState.COMMIT.toString());

      TransactionStateDto transactionStateData = new TransactionStateDto();

      BigDecimal balance = userDao.findBalance(payment.getEmail());
      transactionStateData.setBalance(balance);

      transactionStateData.setState(TransactionState.COMMIT.toString());
      transactionStateData.setTransactionCode(transactionCode);

      this.transactionManager.commit(status);

      return new ResponseEntity<>(transactionStateData, HttpStatus.OK);
    } catch (EmptyResultDataAccessException e) {
      LOGGER.error(e.getMessage(), e);
      return CustomResponseHelper.create(HttpStatus.NOT_FOUND);
    } catch (Exception e) {
      this.transactionManager.rollback(status);
      LOGGER.error(e.getMessage(), e);
      return CustomResponseHelper.create(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

}
