package simpleppob.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingMessage;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.examples.Utils;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;

public interface JsonValidate {

  default <T> void validate(final T data, final String jsonSchema) throws Exception {
    try {
      final JsonNode fstabSchema = Utils.loadResource(jsonSchema);
      final JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
      final JsonSchema schema = factory.getJsonSchema(fstabSchema);

      ObjectMapper mapper = new ObjectMapper();
      final JsonNode dataJson = mapper.valueToTree(data);
      ProcessingReport report = schema.validate(dataJson);
      if (!report.isSuccess()) {
        List<String> errorMessages = new ArrayList<>();
        for (ProcessingMessage processingMessage : report) {
          errorMessages.add(processingMessage.getMessage());
        }

        throw new IllegalArgumentException(errorMessages.toString());
      }
    } catch (IOException | ProcessingException io) {
      throw new Exception(io);
    }
  }

}
