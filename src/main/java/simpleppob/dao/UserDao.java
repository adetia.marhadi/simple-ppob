package simpleppob.dao;

import java.math.BigDecimal;
import simpleppob.dto.UserDto;
import simpleppob.json.SaveUserRequest;

public interface UserDao {

  int save(final SaveUserRequest user);

  UserDto find(final String email);

  boolean validatePassword(final String email, final String password);

  int updateBalance(final String email, final BigDecimal amount);

  BigDecimal findBalance(final String email) throws Exception;

}
