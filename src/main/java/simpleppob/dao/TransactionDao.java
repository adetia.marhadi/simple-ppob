package simpleppob.dao;

import simpleppob.dto.InquiryDto;
import simpleppob.dto.PaymentDto;
import simpleppob.dto.ProductDto;
import simpleppob.json.InquiryDataRequest;

public interface TransactionDao {

  int save(final InquiryDataRequest inquiryData, final ProductDto product);

  InquiryDto findInquiry(final String transactionCode);

  int updateState(final String transactionCode, final String state);

  PaymentDto findPayment(final String transactionCode);

  PaymentDto find(final String transactionCode);

}
