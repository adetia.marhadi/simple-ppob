package simpleppob.dao.impl;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import simpleppob.dao.UserDao;
import simpleppob.dto.UserDto;
import simpleppob.json.SaveUserRequest;

@Repository
public class UserDaoImpl implements UserDao {

  private static final double FIRST_BALANCE = 100000d;

  private static final String EMAIL = "email";

  private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

  @Autowired
  public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
    this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
  }

  @Override
  public int save(SaveUserRequest user) {
    StringBuilder sql = new StringBuilder();
    sql.append("INSERT INTO T_MST_USER (EMAIL, PASSWORD, BALANCE, JOIN_TIME) ")
        .append("VALUES (:email, :password, :balance, NOW())");

    Map<String, Object> params = new HashMap<>();
    params.put(EMAIL, user.getEmail());
    params.put("password", user.getPassword());
    params.put("balance", FIRST_BALANCE);

    try {
      return namedParameterJdbcTemplate.update(sql.toString(), params);
    } catch (DuplicateKeyException d) {
      throw d;
    }
  }

  @Override
  public UserDto find(String email) {
    String sql =
        "SELECT ID, EMAIL, PASSWORD, BALANCE, JOIN_TIME FROM T_MST_USER WHERE EMAIL = :email LIMIT 1";

    Map<String, String> params = new HashMap<>();
    params.put(EMAIL, email);

    try {
      return namedParameterJdbcTemplate.queryForObject(sql, params, (rs, row) -> {
        UserDto userDto = new UserDto();
        userDto.setId(rs.getInt(1));
        userDto.setEmail(rs.getString(2));
        userDto.setPassword(rs.getString(3));
        userDto.setBalance(BigDecimal.valueOf(rs.getDouble(4)));
        userDto.setTimestamp(rs.getString(5));

        return userDto;
      });
    } catch (EmptyResultDataAccessException e) {
      throw e;
    }
  }

  @Override
  public boolean validatePassword(String email, String password) {
    String sql = "SELECT PASSWORD FROM T_MST_USER WHERE EMAIL = :email LIMIT 1";

    Map<String, String> params = new HashMap<>();
    params.put(EMAIL, email);

    String passwordDb;
    try {
      passwordDb =
          namedParameterJdbcTemplate.queryForObject(sql, params, (rs, row) -> rs.getString(1));
    } catch (EmptyResultDataAccessException e) {
      throw e;
    }

    return password.equals(passwordDb);
  }

  @Override
  public int updateBalance(String email, BigDecimal amount) {
    String sql = "UPDATE T_MST_USER SET BALANCE = BALANCE + :amount WHERE EMAIL = :email";

    Map<String, Object> params = new HashMap<>();
    params.put("amount", amount.doubleValue());
    params.put(EMAIL, email);

    return namedParameterJdbcTemplate.update(sql, params);
  }

  @Override
  public BigDecimal findBalance(String email) throws Exception {
    String sql = "SELECT BALANCE FROM T_MST_USER WHERE EMAIL = :email FOR UPDATE";

    Map<String, String> params = new HashMap<>();
    params.put(EMAIL, email);

    try {
      return namedParameterJdbcTemplate.queryForObject(sql, params,
          (rs, row) -> BigDecimal.valueOf(rs.getDouble(1)));
    } catch (EmptyResultDataAccessException e) {
      throw new Exception("balance not found.", e);
    }

  }

}
