package simpleppob.dao;

import simpleppob.dto.ProductDto;

public interface ProductDao {

  ProductDto findByProductCode(final String productCode);

}
