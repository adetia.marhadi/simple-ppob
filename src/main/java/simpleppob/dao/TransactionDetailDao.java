package simpleppob.dao;

import com.fasterxml.jackson.core.JsonProcessingException;
import simpleppob.json.InquiryDataRequest;

public interface TransactionDetailDao {

  int save(final InquiryDataRequest inquiryData, final String state) throws JsonProcessingException;

}
